# megaclisas-status
This a wrapper script around megacli. It was written by Adam Cecile and modiefied by Vincent S. Cojot.

Thanks to Vincent the script now tells the block device file corresponding the RAID array. <br/>

## Sample Output:

```
-- Controller information --
-- ID | H/W Model                  | RAM    | Temp | Firmware     
c0    | LSI MegaRAID SAS 9271-8i   | 1024MB | 88C  | FW: 23.32.0-0009 
c1    | LSI MegaRAID SAS 9280-4i4e | 512MB  | N/A  | FW: 12.15.0-0205 

-- Array information --
-- ID | Type   |    Size |  Strpsz |   Flags | DskCache |  Status |  OS Path | InProgress   
c0u0  | RAID-1 |   1817G |  256 KB | ADRA,WT |  Enabled | Optimal | /dev/sda | None         
c0u1  | RAID-5 |  16370G |  512 KB | ADRA,WB |  Enabled | Optimal | /dev/sdb | None         
c1u0  | RAID-0 |    476G |  256 KB | ADRA,WT |  Enabled | Optimal | /dev/sdc | None         

-- Disk information --
-- ID   | Type | Drive Model                                  | Size     | Status          | Speed    | Temp | Slot ID  | LSI Device ID
c0u0p0  | HDD  | WD-WMC300422505WDC WD20EFRX-68AX9N0 80.00A80 | 1.817 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [252:4]  | 17      
c0u0p1  | HDD  | WD-WMC300421817WDC WD20EFRX-68AX9N0 80.00A80 | 1.817 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [252:5]  | 18      
c0u1p0  | HDD  | WD-WX41DA40LCE5WDC WD60EFRX-68MYMN1 82.00A82 | 5.456 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [252:0]  | 15      
c0u1p1  | HDD  | WD-WX41DA40L42RWDC WD60EFRX-68MYMN1 82.00A82 | 5.456 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [252:1]  | 12      
c0u1p2  | HDD  | WD-WX61DA4HAKFZWDC WD60EFRX-68MYMN1 82.00A82 | 5.456 TB | Online, Spun Up | 6.0Gb/s  | 32C  | [252:6]  | 14      
c0u1p3  | HDD  | WD-WX41DA40LEF1WDC WD60EFRX-68MYMN1 82.00A82 | 5.456 TB | Online, Spun Up | 6.0Gb/s  | 30C  | [252:7]  | 16      
c1u0p0  | SSD  | 0000000011310344CFE3M4-CT512M4SSD2 070H      | 476.4 Gb | Online, Spun Up | 6.0Gb/s  | N/A  | [252:1]  | 11 
```

## How it works:
This script may need some modification depending on where megacli is on your system. <br/>
on line 13: `def_megaclipath = "/opt/MegaRAID/MegaCli/MegaCli64"` <br/>
and on line 75: `os.environ["PATH"] += os.pathsep + '/opt/MegaRAID/MegaCli'` <br/>

### If megacli does not detect your controller you may need this wrapper script around it:
`setarch x86_64 --uname-2.6 /opt/MegaRAID/MegaCli/MegaCli64`
